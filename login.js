document.getElementById("signupForm").addEventListener("submit", function (event) {
    event.preventDefault(); // Prevent form submission to avoid page reload

    // Retrieve form input values
    var username = document.getElementById("username").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;

    // Perform validation or other necessary operations with the collected data
    console.log("Username:", username);
    console.log("Email:", email);
    console.log("Password:", password);

    // Optionally, you can submit the form data to a server using AJAX or other techniques

    // Clear form inputs after submission
    document.getElementById("username").value = "";
    document.getElementById("email").value = "";
    document.getElementById("password").value = "";
});
